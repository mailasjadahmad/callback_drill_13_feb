/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/
const listData = require("./lists_1.json");
const cardsData = require("./cards_1.json");

function exctractingCardsData(id, sendCards) {
  if (!id) {
    return "enter a valid id";
  } else {
    return sendCards(id);
  }
}

function sendCards(id) {
  setTimeout(() => {
    for (let key in listData) {
      for (let innerKey in listData[key]) {
        if ((listData[key][innerKey].id = id)) {
          console.log(cardsData[id]);
          return;
        }
      }
    }
  }, 2000);
}

module.exports = exctractingCardsData;
module.exports = sendCards;
