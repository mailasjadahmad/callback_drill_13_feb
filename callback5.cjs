/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/

const boardInfoFromId = require("./callback1.cjs");
const passBoardInfo = require("./callback1.cjs");
const sendList = require("./callback2.cjs");
const exctractingBoardData = require("./callback2.cjs");
const exctractingCardsData = require("./callback3.cjs");
const sendCards = require("./callback3.cjs");

function callback5() {
  let id = "mcu453ed";
  let listId1 = "qwsa221";
  let listId2 = "jwkh245";
  console.log(boardInfoFromId(id, passBoardInfo));
  console.log(exctractingBoardData(id, sendList));
  console.log(exctractingCardsData(listId1, sendCards));
  console.log(exctractingCardsData(listId2, sendCards));
}

// callback5();
module.exports=callback5