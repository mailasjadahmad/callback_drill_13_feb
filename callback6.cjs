/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const boardInfoFromId = require("./callback1.cjs");
const passBoardInfo = require("./callback1.cjs");
const sendList = require("./callback2.cjs");
const exctractingBoardData = require("./callback2.cjs");
const exctractingCardsData = require("./callback3.cjs");
const sendCards = require("./callback3.cjs");

function callback6() {
  let id = "mcu453ed";

  console.log(boardInfoFromId(id, passBoardInfo));
  const listDataForThanos = exctractingBoardData(id, sendList);
  console.log(listDataForThanos);

  for (let key in listDataForThanos) {
    let listId = listDataForThanos[key].id;
    console.log(exctractingCardsData(listId, sendCards));
  }
}

// callback6();
module.exports=callback6
