/* 
	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
*/

const boardData = require("./boards_1.json");
let id = "mcu453ed";


function boardInfoFromId(id, passBoardInfo) {
  if (!id) {
    return "Enter a valid id";
  } else {
    // console.log("running board fucntion");
    passBoardInfo(id);
  }
}
// console.log(result)
// const id = "mcu453ed";

function passBoardInfo(id) {
  setTimeout(() => {
    for (let key in boardData) {
      // console.log(boardData[key].id)
      if (boardData[key].id == id) {
        let result = boardData[key];
        console.log(result);
      }
    }
  }, 2000);
}

// boardInfoFromId(id, passBoardInfo) 

module.exports = boardInfoFromId;
module.exports = passBoardInfo;
